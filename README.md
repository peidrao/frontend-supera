# FrontEnd SUPERA

## Iniciar projeto

Instalar dependências
```console
purdy@49ers:~$ npm install
```

Iniciar projeto
```console
purdy@49ers:~$ npm start
```

## Pontos de melhoria

- Melhorar layout e experiencia do usuário.
  - Feedbacks nos erros recebidos.
- Otimizar e criar componentes (Tabelas).
- Utilizar hooks para os formulários.

## Tópicos

- [x] - Página de login
- [ ] - Página de criação do usuário
- [x] - Página de pedidos realizado pelo usuário
- [x] - Adição de produtos no carrinho
- [x] - Atualização de preços e quantidade de items no carrinho
- [ ] - Ordenação de produtos pelo preço, popularidade e ordem alfabética
- [ ] - Remocao de pedidos dentro do carrinho
