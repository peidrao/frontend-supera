import { useContext } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
  useLocation,
} from "react-router-dom";
import { CssBaseline } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

import Cart from "./pages/Cart";

import Home from "./pages/Home";
import Header from "./components/Header";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import CheckOut from "./pages/CheckOut";
import Footer from "./components/Footer";

import { AuthContext, AuthProvider } from "./context/AuthContext";
import { Provider } from "./context/GlobalState";

const theme = createTheme();

export function RequireAuth({ children }: { children: JSX.Element }) {
  const { isAuthenticated } = useContext(AuthContext);
  const location = useLocation();
  if (isAuthenticated) {
    return children;
  } else {
    return <Navigate to="/" state={{ from: location }} replace />;
  }
}

export function RedirectOnAuth({ children }: { children: JSX.Element }) {
  const { isAuthenticated } = useContext(AuthContext);
  const location = useLocation();
  if (isAuthenticated) {
    return <Navigate to="/dashboard" state={{ from: location }} replace />;
  }
  return children;
}

function MainRouter() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <Provider>
            <CssBaseline />
            <Header />
            <Routes>
              <Route index path="/" element={<Home />} />
              <Route path="/cart" element={<Cart />} />
              <Route
                path="/login"
                element={
                  <RedirectOnAuth>
                    <Login />
                  </RedirectOnAuth>
                }
              />
              <Route
                path="dashboard"
                element={
                  <RequireAuth>
                    <Dashboard />
                  </RequireAuth>
                }
              ></Route>
              <Route
                path="checkout"
                element={
                  <RequireAuth>
                    <CheckOut />
                  </RequireAuth>
                }
              />
            </Routes>
            <Footer />
          </Provider>
        </ThemeProvider>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default MainRouter;
