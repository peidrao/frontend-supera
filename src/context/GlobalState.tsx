import { createContext, FC, ReactNode, useState, useEffect } from "react";
import ProductService from "../services/ProductService";
import { ProductType } from "../interfaces/ProductInterface";

type ContextType = {
  products?: ProductType[];
  cartItemCount?: number;
  totalPrice?: number;
  tax?: number | 0;
  totalPriceProducts?: number;
  savedItemsCount?: number;
  addToCart?: (product: ProductType) => void;
  removeToCart?: (id: number) => void;
  fetchProducts?: () => Promise<void>;
  isLoading?: boolean;
};

export const GlobalContext = createContext<ContextType>({});

export const Provider: FC<{ children: ReactNode }> = ({ children }) => {
  const [products, setProducts] = useState<ProductType[]>([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalPriceProducts, setTotalPriceProducts] = useState(0);
  const [tax, setTax] = useState(0);
  const [cartItemCount, setCartItemCount] = useState(0);
  const [savedItemsCount, setSavedItemsCount] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  async function fetchProducts() {
    const service = new ProductService();
    const response = await service.listProducts();
    setProducts(response.data);
    setIsLoading(false);
  }

  useEffect(() => {
    fetchProducts();
  }, []);

  useEffect(() => {
    const productsCart = JSON.parse(localStorage.getItem("CART_ITEMS") || "[]");
    const tax = JSON.parse(localStorage.getItem("TOTAL_TAX") || "0");
    const price = JSON.parse(localStorage.getItem("TOTAL_PRICE") || "0");
    const priceProducts = JSON.parse(
      localStorage.getItem("TOTAL_PRICE_PRODUCTS") || "0"
    );

    setTotalPrice(price);
    setTax(tax);
    setTotalPriceProducts(priceProducts);
    setCartItemCount(productsCart.length);

    const savedProducts = products.filter(
      (product) => product.isSaved === true
    );
    setSavedItemsCount(savedProducts.length);
  }, [products]);

  const addToCart = (product: ProductType) => {
    const productsCart = JSON.parse(localStorage.getItem("CART_ITEMS") || "[]");

    let price = JSON.parse(localStorage.getItem("TOTAL_PRICE") || "0");
    let tax = JSON.parse(localStorage.getItem("TOTAL_TAX") || "0");
    let priceProducts = JSON.parse(
      localStorage.getItem("TOTAL_PRICE_PRODUCTS") || "0"
    );

    if (productsCart) {
      if (!productsCart.includes(product.id)) {
        productsCart.push(product.id);
        window.localStorage.setItem("CART_ITEMS", JSON.stringify(productsCart));
        setCartItemCount(productsCart.length);
        price += product.price;
        priceProducts += product.price;

        if (priceProducts >= 250) {
          price = priceProducts;
          tax = 0;
          setTax(0);
          setTotalPrice(priceProducts);
          setTotalPriceProducts(priceProducts);
        } else {
          tax += 10;
          setTotalPriceProducts(price);
          price += tax;
          setTotalPrice(price);
        }

        window.localStorage.setItem("TOTAL_PRICE", price);
        window.localStorage.setItem("TOTAL_TAX", tax);
        window.localStorage.setItem("TOTAL_PRICE_PRODUCTS", priceProducts);
      }
    }
  };

  const removeToCart = (id: number) => {
    setProducts((prevProducts) =>
      prevProducts.map((prevProduct) =>
        prevProduct.id === id ? { ...prevProduct, inCart: false } : prevProduct
      )
    );
  };

  return (
    <GlobalContext.Provider
      value={{
        products,
        cartItemCount,
        totalPrice,
        savedItemsCount,
        totalPriceProducts,
        tax,
        addToCart,
        removeToCart,
        fetchProducts,
        isLoading,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
