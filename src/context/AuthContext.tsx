import { createContext, FC, ReactNode, useState, useEffect } from "react";

type ContextAuthType = {
  isAuthenticated?: boolean;
  signin?: (access: string, referesh: string) => void;
  signout?: () => void;
};

export const AuthContext = createContext<ContextAuthType>({});

export const AuthProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    const accessToken = localStorage.getItem("ACCESS_TOKEN_KEY");
    const refreshToken = localStorage.getItem("REFRESH_TOKEN_KEY");
    if (!refreshToken || !accessToken) {
      console.log("Invalid token");
    } else {
      setIsAuthenticated(true);
    }
  }, []);

  function signin(access: string, refresh: string) {
    localStorage.setItem("ACCESS_TOKEN_KEY", access);
    localStorage.setItem("REFRESH_TOKEN_KEY", refresh);
    setIsAuthenticated(true);
  }

  function signout() {
    localStorage.removeItem("ACCESS_TOKEN_KEY");
    localStorage.removeItem("REFRESH_TOKEN_KEY");
    setIsAuthenticated(false);
  }

  return (
    <AuthContext.Provider
      value={{
        isAuthenticated,
        signin,
        signout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
