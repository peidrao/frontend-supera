import { useEffect, useState } from "react";

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableContainer,
  Typography,
  Container,
  Grid,
  Paper,
} from "@mui/material";

import OrderService from "../../services/OrderService";
import { OrderType } from "../../interfaces/OrderInterface";

const Dashboard = () => {
  const [orders, setOrders] = useState<OrderType[]>([]);

  useEffect(() => {
    const service = new OrderService();
    service
      .meOrders()
      .then((response) => setOrders(response.data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <Container>
      <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          <Typography component="h1" variant="h2" color="text.primary">
            Meus Pedidos
          </Typography>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Codigo</TableCell>
                  <TableCell>Frete</TableCell>
                  <TableCell>Quantidade de produtos</TableCell>
                  <TableCell>Data da Compra</TableCell>
                  <TableCell>Total</TableCell>
                  <TableCell>Status</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {orders.map((order) => (
                  <TableRow key={order.id}>
                    <TableCell>{order.code}</TableCell>
                    <TableCell>{order.tax}</TableCell>
                    <TableCell>{order.quantity}</TableCell>
                    <TableCell>{order.created_at}</TableCell>
                    <TableCell>{order.total}</TableCell>
                    <TableCell>{order.status_order}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Dashboard;
