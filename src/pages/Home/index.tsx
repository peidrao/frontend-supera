import { useContext } from "react";
import { Grid, Container } from "@mui/material";

import ProductItem from "../../components/ProductItem";
import { GlobalContext } from "../../context/GlobalState";

const Home = () => {
  const { products } = useContext(GlobalContext);

  return (
    <main>
      <Container sx={{ py: 8 }} maxWidth="md">
        <Grid container spacing={4}>
          {products!.map((product) => (
            <Grid item key={product.id} xs={12} sm={6} md={4}>
              <ProductItem product={product} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </main>
  );
};

export default Home;
