import { useContext } from "react";
import {
  Typography,
  Button,
  List,
  Container,
  ListItem,
  ListItemText,
  Grid,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import { GlobalContext } from "../../context/GlobalState";
import OrderService from "../../services/OrderService";

const CheckOut = () => {
  const { products, totalPrice, tax } = useContext(GlobalContext);
  const productsCart = JSON.parse(localStorage.getItem("CART_ITEMS") || "[]");
  const navigate = useNavigate();

  function handleCreateOrder() {
    const service = new OrderService();

    service
      .createOrder(tax, totalPrice, productsCart)
      .then((response) => {
        window.localStorage.removeItem("TOTAL_PRICE");
        window.localStorage.removeItem("TOTAL_TAX");
        window.localStorage.removeItem("TOTAL_PRICE_PRODUCTS");
        window.localStorage.removeItem("CART_ITEMS");
        navigate("/dashboard");
      })
      .catch((err) => console.log(err));
  }

  return (
    <Container component="main" maxWidth="sm" sx={{ mb: 4, mt: 2 }}>
      <Typography variant="h6" gutterBottom>
        Pedidos
      </Typography>

      <List disablePadding>
        {products?.map((product) => {
          return productsCart.includes(product.id) ? (
            <ListItem key={product.id} sx={{ py: 1, px: 0 }}>
              <ListItemText primary={product.name} secondary={product.score} />
              <Typography variant="body2">{product.price}</Typography>
            </ListItem>
          ) : (
            <ListItem></ListItem>
          );
        })}

        <ListItem sx={{ py: 1, px: 0 }}>
          <ListItemText primary="Total" />
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            R$ {totalPrice}
          </Typography>
        </ListItem>
      </List>
      <Grid container spacing={2}>
        <Button
          variant="contained"
          onClick={handleCreateOrder}
          endIcon={<CheckCircleOutlineIcon />}
          sx={{ mt: 3, ml: 1, marginLeft: "auto" }}
        >
          Fechar Pedido
        </Button>
      </Grid>
    </Container>
  );
};

export default CheckOut;
