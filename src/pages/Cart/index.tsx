import { useContext } from "react";

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableContainer,
  Typography,
  Container,
  Grid,
  Paper,
  Button,
} from "@mui/material";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import LocalGroceryStoreIcon from "@mui/icons-material/LocalGroceryStore";

import { GlobalContext } from "../../context/GlobalState";
import { Link } from "react-router-dom";

const Cart = () => {
  const { products, totalPrice, totalPriceProducts, tax } =
    useContext(GlobalContext);
  const productsCart = JSON.parse(localStorage.getItem("CART_ITEMS") || "[]");

  const handleClick = (id: number | string) => {
    console.log(id);
  };

  return (
    <Container>
      <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          <Typography component="h1" variant="h2" color="text.primary">
            Meus Pedidos
          </Typography>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Nome</TableCell>
                  <TableCell>Preco</TableCell>
                  <TableCell>Remover</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products?.map((product) => {
                  return productsCart.includes(product.id) ? (
                    <TableRow key={product.id}>
                      <TableCell>{product.name}</TableCell>
                      <TableCell>{`$${product.price}`}</TableCell>
                      <TableCell>
                        <Button
                          variant="text"
                          color="inherit"
                          onClick={() => handleClick(product.id)}
                        >
                          <DeleteForeverIcon color="error" />
                        </Button>
                      </TableCell>
                    </TableRow>
                  ) : (
                    <TableRow></TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <Grid item xs={12} md={4}>
          <Typography component="h1" variant="h2" color="text.primary">
            Preco
          </Typography>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Subtotal</TableCell>
                  <TableCell>Frete</TableCell>
                  <TableCell>Total</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableCell>{totalPriceProducts}</TableCell>
                <TableCell>{tax}</TableCell>
                <TableCell>{totalPrice}</TableCell>
              </TableBody>
            </Table>
          </TableContainer>
          <Button
            sx={{ mt: 3, mb: 2 }}
            variant="contained"
            fullWidth
            endIcon={<LocalGroceryStoreIcon />}
          >
            <Link to="/checkout">Ir para checkout</Link>
          </Button>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Cart;
