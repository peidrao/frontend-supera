import ProfileMeInterface from "../interfaces/ProfileInterface";
import { api } from "../utils/api";

export default class ProfileService {
  async login(
    email: string,
    password: string
  ): Promise<{ status: number; data: { access: string; refresh: string } }> {
    const response = await api.post("api/token/", { email, password });
    return { status: response.status, data: response.data };
  }

  async profileMe(
    token: string
  ): Promise<{ status: number; data: ProfileMeInterface }> {
    const response = await api.post("api/token_is_valid/", { access: token });

    return { status: response.status, data: response.data };
  }
}
