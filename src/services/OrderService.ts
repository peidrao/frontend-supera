import { api } from "../utils/api";

export default class OrderService {
  async createOrder(
    tax?: number,
    price?: number,
    products?: []
  ): Promise<{ status: number; data: [] }> {
    const headers = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("ACCESS_TOKEN_KEY")}`,
      },
    };

    const response = await api.post(
      "api/orders/",
      {
        tax,
        price,
        products,
      },
      headers
    );
    return { status: response.status, data: response.data };
  }

  async meOrders(): Promise<{ status: number; data: [] }> {
    const headers = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("ACCESS_TOKEN_KEY")}`,
      },
    };

    const response = await api.get("api/orders/", headers);
    return { status: response.status, data: response.data };
  }
}
