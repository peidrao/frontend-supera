import { api } from "../utils/api";

export default class ProductService {
  async listProducts(): Promise<{ status: number; data: [] }> {
    const response = await api.get("api/products/");
    return { status: response.status, data: response.data };
  }

  
}
