import { Button, AppBar, Toolbar, Box, Typography } from "@mui/material";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { useContext } from "react";
import { GlobalContext } from "../../context/GlobalState";

import { Link } from "react-router-dom";
import { AuthContext } from "../../context/AuthContext";

const Header = () => {
  const { cartItemCount } = useContext(GlobalContext);
  const { isAuthenticated, signout } = useContext(AuthContext);
  return (
    <AppBar position="static">
      <Toolbar>
        <Box sx={{ flexGrow: 1 }}>
          <Typography variant="h5" component="h2">
            <Link to="/">Loja</Link>
          </Typography>
        </Box>
        <Link to="/cart">
          <Typography component="p">
            <ShoppingCartIcon /> {cartItemCount || 0}
          </Typography>
        </Link>
        {isAuthenticated ? (
          <Box>
            <Link to="/dashboard">
              <Button variant="text" color="inherit">
                DashBoard
              </Button>
            </Link>
            <Button
              variant="text"
              color="inherit"
              onClick={() => {
                if (signout) signout();
              }}
            >
              Sair
            </Button>
          </Box>
        ) : (
          <Link to="/login">
            <Button variant="text" color="inherit">
              Login
            </Button>
          </Link>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Header;
