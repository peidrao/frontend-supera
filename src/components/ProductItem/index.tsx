import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
} from "@mui/material";
import { API_URL } from "../../utils/constants";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import { GlobalContext } from "../../context/GlobalState";
import { ProductType } from "../../interfaces/ProductInterface";
import { useContext } from "react";

type ProductProps = {
  product: ProductType;
};

const ProductItem = ({ product }: ProductProps) => {
  const { addToCart } = useContext(GlobalContext);
  return (
    <Card
      sx={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <CardMedia
        component="img"
        image={`${API_URL}static/${product.image}`}
        alt="random"
      />
      <CardContent sx={{ flexGrow: 1 }}>
        <Typography gutterBottom variant="h5" component="h2">
          {product.name}
        </Typography>
        <Typography>R$ {product.price}</Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          onClick={() => {
            addToCart!(product);
          }}
        >
          <AddShoppingCartIcon />
        </Button>
      </CardActions>
    </Card>
  );
};

export default ProductItem;
