export default interface ProfileMeInterface {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  username: string;
}
