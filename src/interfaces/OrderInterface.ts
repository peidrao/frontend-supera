export interface OrderType {
  id: number;
  code: string;
  tax: number;
  total: number;
  quantity: number;
  created_at: string;
  status_order: string;
}
