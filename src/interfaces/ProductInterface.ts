export interface ProductType {
  id: number | string;
  score: number;
  name: string;
  price: number;
  image: string;
  isSaved?: boolean;
  inCart?: boolean;
  quantity?: number | string;
}
